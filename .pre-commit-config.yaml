---
minimum_pre_commit_version: 2.9.2
repos:
  - repo: https://github.com/pre-commit/pre-commit-hooks
    rev: v4.0.1
    hooks:
      - id: check-merge-conflict  # Check for files that contain merge conflict strings.
      - id: trailing-whitespace
        args: [--markdown-linebreak-ext=md]
        exclude: '.*\.rtf'
      - id: mixed-line-ending     # Replaces or checks mixed line ending.
        args: [--fix=lf]
      - id: end-of-file-fixer
        exclude: '.*\.rtf'
      - id: fix-encoding-pragma
        args: [--remove]
      - id: check-yaml
      - id: debug-statements
        language_version: python3

  # ----- Formatting ---------------------------------------------------------------------------->
  - repo: https://github.com/saltstack/pre-commit-remove-import-headers
    rev: 1.0.0
    hooks:
      - id: remove-import-headers

  - repo: https://github.com/asottile/pyupgrade
    rev: v2.29.1
    hooks:
      - id: pyupgrade
        name: Rewrite Code to be Py3.6+
        args: [--py36-plus]

  - repo: https://github.com/asottile/reorder_python_imports
    rev: v2.6.0
    hooks:
      - id: reorder-python-imports
        args: [--py36-plus]

  - repo: https://github.com/psf/black
    rev: 21.11b1
    hooks:
      - id: black
        args: []
        additional_dependencies:
          - click<8.1.0

  - repo: https://github.com/asottile/blacken-docs
    rev: v1.7.0
    hooks:
      - id: blacken-docs
        additional_dependencies:
          - click<8.1.0
          - black==21.11b1
  # <---- Formatting -----------------------------------------------------------------------------

  # ----- Static Analysis ----------------------------------------------------------------------->
#  - repo: https://github.com/pre-commit/mirrors-mypy
#
#    rev: v0.782
#    hooks:
#      - id: mypy
#        files: ^.*\.py$
#        args: []
#  # <---- Static Analysis ------------------------------------------------------------------------

  # ----- Static Testing Requirements ----------------------------------------------------------->
  - repo: https://github.com/saltstack/pip-tools-compile-impersonate
    rev: "4.4"
    hooks:
      - id: pip-tools-compile
        alias: compile-3.6-test-requirements
        name: Py3.6 Linux Test Requirements
        files: ^requirements/(tests\.in|py3\.6/tests\.txt)$
        pass_filenames: false
        args:
          - -v
          - --py-version=3.6
          - --platform=linux
          - requirements/tests.in

  - repo: https://github.com/saltstack/pip-tools-compile-impersonate
    rev: "4.8"
    hooks:
      - id: pip-tools-compile
        alias: compile-3.7-test-requirements
        name: Py3.7 Linux Test Requirements
        files: ^requirements/(tests\.in|py3\.7/tests\.txt)$
        pass_filenames: false
        args:
          - -v
          - --py-version=3.7
          - --platform=linux
          - requirements/tests.in

  - repo: https://github.com/saltstack/pip-tools-compile-impersonate
    rev: "4.8"
    hooks:
      - id: pip-tools-compile
        alias: compile-3.7-test-requirements
        name: Py3.7 Darwin Test Requirements
        files: ^requirements/(tests\.in|py3\.7/darwin\.txt)$
        pass_filenames: false
        args:
          - -v
          - --py-version=3.7
          - --platform=darwin
          - requirements/darwin.in

  - repo: https://github.com/saltstack/pip-tools-compile-impersonate
    rev: "4.8"
    hooks:
      - id: pip-tools-compile
        alias: compile-3.7-windows-requirements
        name: Py3.7 Windows Test Requirements
        files: ^requirements/(tests\.in|py3\.8/windows\.txt)$
        pass_filenames: false
        args:
          - -v
          - --py-version=3.7
          - --platform=windows
          - requirements/windows.in

  - repo: https://github.com/saltstack/pip-tools-compile-impersonate
    rev: "4.8"
    hooks:
      - id: pip-tools-compile
        alias: compile-3.8-test-requirements
        name: Py3.8 Linux Test Requirements
        files: ^requirements/(tests\.in|py3\.8/tests\.txt)$
        pass_filenames: false
        args:
          - -v
          - --py-version=3.8
          - --platform=linux
          - requirements/tests.in

  - repo: https://github.com/saltstack/pip-tools-compile-impersonate
    rev: "4.8"
    hooks:
      - id: pip-tools-compile
        alias: compile-3.8-test-requirements
        name: Py3.8 Darwin Test Requirements
        files: ^requirements/(tests\.in|py3\.8/darwin\.txt)$
        pass_filenames: false
        args:
          - -v
          - --py-version=3.8
          - --platform=darwin
          - requirements/darwin.in

  - repo: https://github.com/saltstack/pip-tools-compile-impersonate
    rev: "4.8"
    hooks:
      - id: pip-tools-compile
        alias: compile-3.8-windows-requirements
        name: Py3.8 Windows Test Requirements
        files: ^requirements/(tests\.in|py3\.8/windows\.txt)$
        pass_filenames: false
        args:
          - -v
          - --py-version=3.8
          - --platform=windows
          - requirements/windows.in

  - repo: https://github.com/saltstack/pip-tools-compile-impersonate
    rev: "4.4"
    hooks:
      - id: pip-tools-compile
        alias: compile-3.9-test-requirements
        name: Py3.9 Test Requirements
        files: ^requirements/(tests\.in|py3\.9/tests\.txt)$
        pass_filenames: false
        args:
          - -v
          - --py-version=3.9
          - --platform=linux
          - requirements/tests.in

  - repo: https://github.com/saltstack/pip-tools-compile-impersonate
    rev: "4.8"
    hooks:
      - id: pip-tools-compile
        alias: compile-3.9-test-requirements
        name: Py3.9 Darwin Test Requirements
        files: ^requirements/(tests\.in|py3\.9/darwin\.txt)$
        pass_filenames: false
        args:
          - -v
          - --py-version=3.9
          - --platform=darwin
          - requirements/darwin.in

  - repo: https://github.com/saltstack/pip-tools-compile-impersonate
    rev: "4.8"
    hooks:
      - id: pip-tools-compile
        alias: compile-3.9-windows-requirements
        name: Py3.9 Windows Test Requirements
        files: ^requirements/(tests\.in|py3\.8/windows\.txt)$
        pass_filenames: false
        args:
          - -v
          - --py-version=3.9
          - --platform=windows
          - requirements/windows.in

  - repo: https://github.com/saltstack/pip-tools-compile-impersonate
    rev: "4.8"
    hooks:
      - id: pip-tools-compile
        alias: compile-3.10-test-requirements
        name: Py3.10 Linux Test Requirements
        files: ^requirements/(tests\.in|py3\.10/tests\.txt)$
        pass_filenames: false
        args:
          - -v
          - --py-version=3.10
          - --platform=linux
          - requirements/tests.in

  - repo: https://github.com/saltstack/pip-tools-compile-impersonate
    rev: "4.8"
    hooks:
      - id: pip-tools-compile
        alias: compile-3.10-test-requirements
        name: Py3.10 Darwin Test Requirements
        files: ^requirements/(tests\.in|py3\.10/darwin\.txt)$
        pass_filenames: false
        args:
          - -v
          - --py-version=3.10
          - --platform=darwin
          - requirements/darwin.in

  - repo: https://github.com/saltstack/pip-tools-compile-impersonate
    rev: "4.8"
    hooks:
      - id: pip-tools-compile
        alias: compile-3.10-windows-requirements
        name: Py3.10 Windows Test Requirements
        files: ^requirements/(tests\.in|py3\.8/windows\.txt)$
        pass_filenames: false
        args:
          - -v
          - --py-version=3.10
          - --platform=windows
          - requirements/windows.in
  # <---- Static Testing Requirements ------------------------------------------------------------
